# Charset
  export LC_CTYPE=en_US.UTF-8

# Editor setup
  export EDITOR=vim

# GPG
  # Export default GPG key
    export GPGKEY=24E70CB33185172C
  # GPG should use a real tty
    export GPG_TTY=$(tty)

# Load aliases and shortcuts if existent.
  function load_aliases {
    [ -f "$HOME/.aliasrc" ] && source "$HOME/.aliasrc"
    [ -f "$HOME/.zsh/aliasrc" ] && source "$HOME/.zsh/aliasrc"
  }

# Print all unique X11 'DISPLAY' values for a specific user
  function show_displays {
  	ps e | sed -rn 's/.* DISPLAY=([.:0-9a-zA-Z]*) .*/\1/p' | uniq
  }

# Keychain
  function keychain_start {
    if command -v keychain >/dev/null 2>&1; then
      [[ -f ~/.keychain/"$(hostname)"-sh ]] && source ~/.keychain/"$(hostname)"-sh  
      keychain --timeout 240
    fi
  }    

# Change cursor shape for different vi modes.
  function zle-keymap-select {
   if [[ ${KEYMAP} == vicmd ]] ||
       [[ $1 = 'block' ]]; then
      echo -ne '\e[1 q'
    elif [[ ${KEYMAP} == main ]] ||
         [[ ${KEYMAP} == viins ]] ||
         [[ ${KEYMAP} = '' ]] ||
         [[ $1 = 'beam' ]]; then
      echo -ne '\e[5 q'
    fi
  }

# Set initial cursor shape and initial keymap
  zle-line-init() {
    zle -K viins   
    echo -ne "\e[5 q"
  }

# Set vi mode
  bindkey -v
  export KEYTIMEOUT=1
  # Backspace deletes character
  bindkey -v '^?' backward-delete-char

# Load aliases
  load_aliases

# History
  HISTSIZE=10000
  SAVEHIST=10000
  HISTFILE=~/.zhistory
  # Enable searching through history
    bindkey '^r' history-incremental-pattern-search-backward

# Autotab complete
  autoload -U compinit
  zstyle ':completion:*' menu select
  zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
  zmodload zsh/complist
  compinit
  _comp_options+=(globdots)		# Include hidden files.

# Edit line in vi(m) buffer with CTRL+v
  autoload edit-command-line 
  zle -N edit-command-line
  bindkey '^v' edit-command-line
  bindkey -M vicmd "^v" edit-command-line

# Use vi(m) keys and arrow keys to navigate over suggestions menu
  bindkey -M menuselect 'h' vi-backward-char
  bindkey -M menuselect 'j' vi-down-line-or-history
  bindkey -M menuselect 'k' vi-up-line-or-history
  bindkey -M menuselect 'l' vi-forward-char
  bindkey -M menuselect 'left' vi-backward-char
  bindkey -M menuselect 'down' vi-down-line-or-history
  bindkey -M menuselect 'up' vi-up-line-or-history
  bindkey -M menuselect 'right' vi-forward-char

# Accept suggestions with CRTL+<SPACE> 
  bindkey '^ ' autosuggest-accept

# Set cursor shapes
  zle -N zle-line-init
  zle -N zle-keymap-select

# Source plugins  
  source .zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
  source .zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null

# Prompt setup
  autoload -U promptinit
  promptinit
  prompt walters

# Start Keychain
  keychain_start
