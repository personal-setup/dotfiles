" Basic remappings
inoremap jk <ESC>
let mapleader=","

" Compatibility and interoperability
set encoding=utf-8
set clipboard=unnamedplus
set nocompatible

" Layout
filetype plugin indent on
syntax on
set modelines=0
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set scrolloff=3
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set number
set relativenumber
set noundofile
set cursorline

" Search 
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
nnoremap <leader><space> :noh<cr>

nnoremap <tab> %
vnoremap <tab> %
set wrap
set linebreak
set nolist
set colorcolumn=80
