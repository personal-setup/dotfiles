#!/usr/bin/env zsh

set -Eeuso pipefail

function make-link {
  [[ ! -e "$2" ]] && \
    src_dirname=$(dirname "$2") && \
    mkdir -p "$src_dirname" 
  rp_src=$(realpath -s "$2")
  rp_dest=$(realpath -s "$1")
  [[ -L "$rp_src" ]] && rm "$rp_src"
  [[ -d "$rp_src" || -f "$rp_src" ]] && \
    hash_src=$(tar -c "$rp_src" 2>/dev/null | md5sum | cut -f1 -d " ") && \
    mv "$rp_src" "$rp_src.$hash_src.bkp"
  ln -s "$rp_dest" "$rp_src"
}

# git files
make-link .gitconfig ~/.gitconfig

# zsh and bash aliases
make-link .aliasrc ~/.aliasrc

# zsh files
make-link .zshrc ~/.zshrc
make-link .zsh ~/.zsh

# bash files
make-link .bash_profile ~/.bash_profile
make-link .bashrc ~/.bashrc

# gnupg files
make-link .gnupg/gpg.conf ~/.gnupg/gpg.conf

# vim files
make-link .vimrc ~/.vimrc
